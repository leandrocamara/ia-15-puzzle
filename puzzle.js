const MOVIMENTO_PARA_CIMA = 1
const MOVIMENTO_PARA_DIREITA = 2
const MOVIMENTO_PARA_BAIXO = 3
const MOVIMENTO_PARA_ESQUERDA = 4

function Puzzle() {

    this.tamanhoMatriz
    this.matrizQuebraCabeca = []
    this.custoTotalEstadoAtualPuzzle
    this.qtdMovimentos = 0

    /**
     *
     */
    this.inicializarMatriz = function () {
        var valorQuadrado = 1

        for (var linha = 0; linha < this.getTamanhoMatriz(); linha++) {
            this.getMatrizQuebraCabeca()[linha] = new Array(this.getTamanhoMatriz())

            for (var coluna = 0; coluna < this.getMatrizQuebraCabeca()[linha].length; coluna++) {
                this.getMatrizQuebraCabeca()[linha][coluna] = new Quadrado(valorQuadrado, linha, coluna)

                valorQuadrado++
            }
        }

        this.esvaziarQuadrado(this.getMatrizQuebraCabeca()[this.getTamanhoMatriz() - 1][this.getTamanhoMatriz() - 1])
    }

    /**
     *
     */
    this.embaralharQuebraCabeca = function (quantidadePassosAleatorios) {

        for (var passo = 0; passo < quantidadePassosAleatorios; passo++) {

            var quadradoVazio = this.getQuadradoPorValor(null)

            var movimento = this.getDirecaoAleatoria(quadradoVazio)

            this.substituirQuadrados(quadradoVazio, movimento)
        }

        this.visualizarQuebraCabeca()
    }

    /**
     *
     */
    this.resolverPuzzle = function () {

        var quantQuadradosMatriz = Math.pow(this.getTamanhoMatriz(), 2) - 1

        this.qtdMovimentos = 0
        for (var valor = 1; valor <= quantQuadradosMatriz; valor++) {

            var quadradoOriginal = this.getQuadradoPorValor(valor)

            var quadradoAuxiliar = new Quadrado(quadradoOriginal.getValor(), quadradoOriginal.getLinhaOrigem(), quadradoOriginal.getColunaOrigem())

            quadradoAuxiliar.setLinhaAtual(quadradoOriginal.getLinhaAtual())
            quadradoAuxiliar.setColunaAtual(quadradoOriginal.getColunaAtual())

            var movimentacoesPossiveis = this.movimentacoesPossiveisQuadrado(quadradoAuxiliar)

            var posicaoDestino = {
                linha: quadradoAuxiliar.getLinhaOrigem(),
                coluna: quadradoAuxiliar.getColunaOrigem()
            }

            // Verifica se a peça já está na posição correta.
            if (quadradoAuxiliar.getLinhaOrigem() == quadradoAuxiliar.getLinhaAtual() && quadradoAuxiliar.getColunaOrigem() == quadradoAuxiliar.getColunaAtual()) {
              continue
            }

            var melhorCaminho = this.getMelhorCaminhoParaPosicaoOrigem(quadradoAuxiliar, posicaoDestino, movimentacoesPossiveis)

            for (var caminho = 0; caminho < melhorCaminho.length; caminho++) {

                posicaoDestino = {
                    linha: melhorCaminho[caminho].linha,
                    coluna: melhorCaminho[caminho].coluna
                }
                
                var quadradoVazio = this.getQuadradoPorValor(null)

                var quadradoVazioAuxiliar = new Quadrado(quadradoVazio.getValor(), posicaoDestino.linha, posicaoDestino.coluna)
    
                quadradoVazioAuxiliar.setLinhaAtual(quadradoVazio.getLinhaAtual())
                quadradoVazioAuxiliar.setColunaAtual(quadradoVazio.getColunaAtual())

                movimentacoesPossiveis = this.movimentacoesPossiveisQuadrado(quadradoVazioAuxiliar)

                var posicaoDesconsiderada = {
                    linha: quadradoOriginal.getLinhaAtual(),
                    coluna: quadradoOriginal.getColunaAtual()
                }

                // Verifica se a peça não está já na posição correta.
                if (! (posicaoDestino.linha == quadradoVazioAuxiliar.getLinhaAtual() && posicaoDestino.coluna == quadradoVazioAuxiliar.getColunaAtual()) ) {

                    var melhorCaminhoQuadradoVazio = this.getMelhorCaminhoQuadradoVazioParaPosicaoDestino(quadradoVazioAuxiliar, posicaoDestino, movimentacoesPossiveis, posicaoDesconsiderada)

                    for (var index = 0; index < melhorCaminhoQuadradoVazio.length; index++) {
                        var novaPosicao = melhorCaminhoQuadradoVazio[index]

                        var quadradoAdjascente = this.getQuadradoPorLinhaColuna(novaPosicao.linha, novaPosicao.coluna)

                        this.alterarPosicaoEntreQuadrados(quadradoVazio, quadradoAdjascente)
                        this.qtdMovimentos++
                    }
                }
                
                this.alterarPosicaoEntreQuadrados(quadradoVazio, quadradoOriginal)
                this.qtdMovimentos++
            }

            // for (var index = 1; index <= valor; index++) {
            //     console.log(index)
            //     var quadrado = this.getQuadradoPorValor(index)

            //     if (! (quadrado.getLinhaOrigem() == quadrado.getLinhaAtual() && quadrado.getColunaOrigem() == quadrado.getColunaAtual()) ) {
            //         valor = index
            //         return
            //     }
            // }
        }

        this.visualizarQuebraCabeca()
    }

    /**
     *
     */
    this.visualizarQuebraCabeca = function () {

      var string = "<h2>15 Puzzle</h2><table border='1px solid #ccc' style='font-size: 30px; text-align: center'>"

        for (var linha = 0; linha < this.getTamanhoMatriz(); linha++) {

            string += '<tr>'

            for (var coluna = 0; coluna < this.getTamanhoMatriz(); coluna++) {
                string += '<td style="width: 50px">' + this.getQuadradoPorLinhaColuna(linha, coluna).getValor() + '</td>'
            }

            string += '</tr>'

        }

        string += '</table>'

        string += '<div style="font-size: 20px">Quantidade de movimentos: '+ this.qtdMovimentos +'</div>'

        document.getElementById("board").innerHTML = string
    }

    /**
     *
     */
    this.getMelhorCaminhoQuadradoVazioParaPosicaoDestino = function (quadrado, posicaoDestino, movimentacoesPossiveis, posicaoDesconsiderada = null) {

              var melhorCaminho = new Array()

              var menorCustoTotalPossivel = 9999

              var posicaoAtual = {
                  linha: quadrado.getLinhaAtual(),
                  coluna: quadrado.getColunaAtual()
              }

              var posicaoAnterior = {
                  linha: null,
                  coluna: null
              }

              while ( JSON.stringify(posicaoAtual) !== JSON.stringify(posicaoDestino) ) {

                  for (var index = 0; index < movimentacoesPossiveis.length; index++) {

                      var movimento = movimentacoesPossiveis[index]

                      var novaPosicao = this.getNovaPosicaoPorQuadradoEDirecao(quadrado, movimento)

                      if (JSON.stringify(novaPosicao) !== JSON.stringify(posicaoDesconsiderada) && JSON.stringify(novaPosicao) !== JSON.stringify(posicaoAnterior)) {

                          var quadradoAuxiliar = new Quadrado(quadrado.getValor(), posicaoDestino.linha, posicaoDestino.coluna)

                          quadradoAuxiliar.setLinhaAtual(novaPosicao.linha)
                          quadradoAuxiliar.setColunaAtual(novaPosicao.coluna)
                          quadradoAuxiliar.calcularFuncaoHeuristica()

                          var custoH = quadradoAuxiliar.getCustoH()

                          if (custoH < menorCustoTotalPossivel) {
                              menorCustoTotalPossivel = custoH
                              posicaoAtual = novaPosicao
                          }
                      }
                  }

                  posicaoAnterior.linha = quadrado.getLinhaAtual()
                  posicaoAnterior.coluna = quadrado.getColunaAtual()

                  quadrado.setLinhaAtual(posicaoAtual.linha)
                  quadrado.setColunaAtual(posicaoAtual.coluna)

                  movimentacoesPossiveis = this.movimentacoesPossiveisQuadrado(quadrado)

                  melhorCaminho.push(posicaoAtual)
              }

              return melhorCaminho
          }

    /**
     *
     */
    this.getMelhorCaminhoParaPosicaoOrigem = function (quadrado, posicaoDestino, movimentacoesPossiveis) {

        var melhorCaminho = new Array()

        var menorCustoTotalPossivel = 9999

        var posicaoAtual = {
            linha: quadrado.getLinhaAtual(),
            coluna: quadrado.getColunaAtual()
        }

        while ( JSON.stringify(posicaoAtual) !== JSON.stringify(posicaoDestino) ) {

            for (var index = 0; index < movimentacoesPossiveis.length; index++) {

                var movimento = movimentacoesPossiveis[index]

                var novaPosicao = this.getNovaPosicaoPorQuadradoEDirecao(quadrado, movimento)

                var quadradoAuxiliar = new Quadrado(quadrado.getValor(), posicaoDestino.linha, posicaoDestino.coluna)

                quadradoAuxiliar.setLinhaAtual(novaPosicao.linha)
                quadradoAuxiliar.setColunaAtual(novaPosicao.coluna)
                quadradoAuxiliar.calcularFuncaoHeuristica()

                var custoH = quadradoAuxiliar.getCustoH()

                if (custoH < menorCustoTotalPossivel) {
                    menorCustoTotalPossivel = custoH
                    posicaoAtual = novaPosicao
                }
            }

            quadrado.setLinhaAtual(posicaoAtual.linha)
            quadrado.setColunaAtual(posicaoAtual.coluna)

            movimentacoesPossiveis = this.movimentacoesPossiveisQuadrado(quadrado)

            melhorCaminho.push(posicaoAtual)
        }

        return melhorCaminho
    }

    /**
     *
     */
    this.substituirQuadrados = function (quadradoVazio, movimento) {

        var novaPosicao = this.getNovaPosicaoPorQuadradoEDirecao(quadradoVazio, movimento)

        var quadradoAdjascente = this.getQuadradoPorLinhaColuna(novaPosicao.linha, novaPosicao.coluna)

        this.alterarPosicaoEntreQuadrados(quadradoVazio, quadradoAdjascente)
    }

    /**
     *
     */
    this.movimentacoesPossiveisQuadrado = function (quadrado) {
        var opcoesDirecao = this.getOpcoesDirecao()

        if (!this.podeMoverParaEsquerda(quadrado)) {
            this.removerOpcaoDirecoes(opcoesDirecao, MOVIMENTO_PARA_ESQUERDA)
        }

        if (!this.podeMoverParaCima(quadrado)) {
            this.removerOpcaoDirecoes(opcoesDirecao, MOVIMENTO_PARA_CIMA)
        }

        if (!this.podeMoverParaDireita(quadrado)) {
            this.removerOpcaoDirecoes(opcoesDirecao, MOVIMENTO_PARA_DIREITA)
        }

        if (!this.podeMoverParaBaixo(quadrado)) {
            this.removerOpcaoDirecoes(opcoesDirecao, MOVIMENTO_PARA_BAIXO)
        }

        return opcoesDirecao
    }

    /**
     *
     */
    this.getDirecaoAleatoria = function (quadradoVazio) {

        var opcoesDirecao = this.movimentacoesPossiveisQuadrado(quadradoVazio)

        return opcoesDirecao[Math.floor((Math.random() * opcoesDirecao.length))]
    }

    /**
     *
     */
    this.getNovaPosicaoPorQuadradoEDirecao = function (quadrado, direcao) {

        var novaPosicao = {
            linha: null,
            coluna: null
        }

        switch (direcao) {
            case MOVIMENTO_PARA_ESQUERDA:
                novaPosicao.linha = quadrado.getLinhaAtual()
                novaPosicao.coluna = quadrado.getColunaAtual() - 1
                break

            case MOVIMENTO_PARA_CIMA:
                novaPosicao.linha = quadrado.getLinhaAtual() - 1
                novaPosicao.coluna = quadrado.getColunaAtual()
                break

            case MOVIMENTO_PARA_DIREITA:
                novaPosicao.linha = quadrado.getLinhaAtual()
                novaPosicao.coluna = quadrado.getColunaAtual() + 1
                break

            case MOVIMENTO_PARA_BAIXO:
                novaPosicao.linha = quadrado.getLinhaAtual() + 1
                novaPosicao.coluna = quadrado.getColunaAtual()
                break
        }

        return novaPosicao
    }

    /**
     *
     */
    this.alterarPosicaoEntreQuadrados = function (quadradoVazio, quadradoAdjascente) {

        var linhaOrigemQuadradoAdjascente = quadradoAdjascente.getLinhaOrigem()
        var colunaOrigemQuadradoAdjascente = quadradoAdjascente.getColunaOrigem()
        var linhaAtualQuadradoAdjascente = quadradoAdjascente.getLinhaAtual()
        var colunaAtualQuadradoAdjascente = quadradoAdjascente.getColunaAtual()
        
        var linhaOrigemQuadradoVazio = quadradoVazio.getLinhaOrigem()
        var colunaOrigemQuadradoVazio = quadradoVazio.getColunaOrigem()
        var linhaAtualQuadradoVazio = quadradoVazio.getLinhaAtual()
        var colunaAtualQuadradoVazio = quadradoVazio.getColunaAtual()

        quadradoAdjascente.setLinhaOrigem(linhaOrigemQuadradoVazio)
        quadradoAdjascente.setLinhaOrigem(colunaOrigemQuadradoVazio)
        quadradoAdjascente.setLinhaAtual(linhaAtualQuadradoVazio)
        quadradoAdjascente.setColunaAtual(colunaAtualQuadradoVazio)
        
        quadradoVazio.setLinhaOrigem(linhaOrigemQuadradoAdjascente)
        quadradoVazio.setLinhaOrigem(colunaOrigemQuadradoAdjascente)
        quadradoVazio.setLinhaAtual(linhaAtualQuadradoAdjascente)
        quadradoVazio.setColunaAtual(colunaAtualQuadradoAdjascente)

        this.getMatrizQuebraCabeca()[linhaAtualQuadradoAdjascente][colunaAtualQuadradoAdjascente] = quadradoVazio
        this.getMatrizQuebraCabeca()[linhaAtualQuadradoVazio][colunaAtualQuadradoVazio] = quadradoAdjascente
    }

    /**
     * Verifica se à esquerda do espaço vazio ultrapassa o limite da matriz.
     */
    this.podeMoverParaEsquerda = function (quadradoVazio) {
        return (quadradoVazio.getColunaAtual() - 1) >= this.getLimiteEsquerda()
    }

    /**
     * Verifica se acima do espaço vazio ultrapassa o limite da matriz.
     */
    this.podeMoverParaCima = function (quadradoVazio) {
        return (quadradoVazio.getLinhaAtual() - 1) >= this.getLimiteAcima()
    }

    /**
     * Verifica se à direita do espaço vazio ultrapassa o limite da matriz.
     */
    this.podeMoverParaDireita = function (quadradoVazio) {
        return (quadradoVazio.getColunaAtual() + 1) <= this.getLimiteDireita()
    }

    /**
     * Verifica se abaixo do espaço vazio ultrapassa o limite da matriz.
     */
    this.podeMoverParaBaixo = function (quadradoVazio) {
        return (quadradoVazio.getLinhaAtual() + 1) <= this.getLimiteAbaixo()
    }

    /**
     *
     */
    this.getTamanhoMatriz = function () {
        return this.tamanhoMatriz
    }

    /**
     *
     */
    this.setTamanhoMatriz = function (tamanhoMatriz) {
        this.tamanhoMatriz = tamanhoMatriz
    }

    /**
     *
     */
    this.getMatrizQuebraCabeca = function () {
        return this.matrizQuebraCabeca
    }

    /**
     *
     */
    this.setMatrizQuebraCabeca = function (matrizQuebraCabeca) {
        this.matrizQuebraCabeca = matrizQuebraCabeca
    }

    /**
     *
     */
    this.getCustoTotalEstadoAtualPuzzle = function () {
        return this.custoTotalEstadoAtualPuzzle
    }

    /**
     *
     */
    this.setCustoTotalEstadoAtualPuzzle = function (custoTotalEstadoAtualPuzzle) {
        this.custoTotalEstadoAtualPuzzle = custoTotalEstadoAtualPuzzle
    }

    /**
     *
     */
    this.getLimiteEsquerda = function () {
        return 0
    }

    /**
     *
     */
    this.getLimiteAcima = function () {
        return 0
    }

    /**
     *
     */
    this.getLimiteDireita = function () {
        return this.getTamanhoMatriz() - 1
    }

    /**
     *
     */
    this.getLimiteAbaixo = function () {
        return this.getTamanhoMatriz() - 1
    }

    /**
     *
     */
    this.getQuadradoPorLinhaColuna = function (linha, coluna) {
        return this.getMatrizQuebraCabeca()[linha][coluna]
    }

    /**
     *
     */
    this.getQuadradoPorValor = function (valor) {

        for (var linha = 0; linha < this.getTamanhoMatriz(); linha++) {

            for (var coluna = 0; coluna < this.getTamanhoMatriz(); coluna++) {

                var quadrado = this.getQuadradoPorLinhaColuna(linha, coluna)

                if (quadrado.getValor() == valor) {
                    return quadrado
                }

            }
        }
    }

    /**
     *
     */
    this.esvaziarQuadrado = function (quadrado) {
        quadrado.setValor(null)
        quadrado.setVazio(true)
    }

    /**
     *
     */
    this.getOpcoesDirecao = function () {
        return [
            MOVIMENTO_PARA_CIMA,
            MOVIMENTO_PARA_DIREITA,
            MOVIMENTO_PARA_BAIXO,
            MOVIMENTO_PARA_ESQUERDA
        ]
    }

    /**
     *
     */
    this.removerOpcaoDirecoes = function (opcoes, direcao) {
        opcoes.splice(opcoes.indexOf(direcao), 1)
    }
}
