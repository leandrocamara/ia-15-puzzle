function Quadrado (valor, linhaOrigem, colunaOrigem) {

    this.custoG = 1
    this.custoH = 0
    this.valor = valor
    this.linhaAtual = linhaOrigem
    this.colunaAtual = colunaOrigem
    this.linhaOrigem = linhaOrigem
    this.colunaOrigem = colunaOrigem
    this.vazio = false

    this.getCustoG = function () {
        return this.custoG
    }

    this.setCustoG = function (custoG) {
        this.custoG = custoG
    }

    this.getCustoH = function () {
        return this.custoH
    }

    this.setCustoH = function (custoH) {
        this.custoH = custoH
    }

    this.getValor = function () {
        return this.valor
    }

    this.setValor = function (valor) {
        this.valor = valor
    }

    this.getLinhaAtual = function () {
        return this.linhaAtual
    }

    this.setLinhaAtual = function (linhaAtual) {
        this.linhaAtual = linhaAtual
    }

    this.getColunaAtual = function () {
        return this.colunaAtual
    }

    this.setColunaAtual = function (colunaAtual) {
        this.colunaAtual = colunaAtual
    }

    this.getLinhaOrigem = function () {
        return this.linhaOrigem
    }

    this.setLinhaOrigem = function (LinhaOrigem) {
        this.linhaOrigem = linhaOrigem
    }

    this.getColunaOrigem = function () {
        return this.colunaOrigem
    }

    this.setColunaOrigem = function (colunaOrigem) {
        this.colunaOrigem = colunaOrigem
    }

    this.isVazio = function () {
        return this.vazio
    }

    this.setVazio = function (vazio) {
        this.vazio = vazio
    }

    /**
     * Distância de Manhattan (custoH)
     * |x1 - x2| + |y1 - y2|
     */
    this.calcularFuncaoHeuristica = function () {
        var distanciaEixoX = Math.abs(this.getLinhaOrigem() - this.getLinhaAtual())
        var distanciaEixoY = Math.abs(this.getColunaOrigem() - this.getColunaAtual())

        this.setCustoH(distanciaEixoX + distanciaEixoY)
    }

    /**
     * f(n) = g(n) + h(n)
     */
    this.getCustoF = function () {
        return (this.getCustoG() + this.getCustoH())
    }
};
