const TAMANHO_MATRIZ = 4
const QUANTIDADE_PASSOS_ALEATORIOS = 30

function Main () {

    /**
     *
     */
    this.iniciarJogo15Puzzle = function () {

        var puzzle = new Puzzle()

        puzzle.setTamanhoMatriz(TAMANHO_MATRIZ)
        puzzle.inicializarMatriz()

        puzzle.visualizarQuebraCabeca()

        setTimeout(function() {
            puzzle.embaralharQuebraCabeca(QUANTIDADE_PASSOS_ALEATORIOS)

            setTimeout(function() {
                puzzle.resolverPuzzle()
            }, 500);

        }, 500);

    }
}
